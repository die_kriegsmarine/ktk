package filegc

import (
	"github.com/stretchr/testify/assert"
	"os"
	"testing"
	"time"
)

func Test001_RetentionNoConfigFileInitially(t *testing.T) {
	ConfigFileName = "bad_retention_policy.json"
	r := NewRetentionPolicy()
	assert.Error(t, r.Load())
}

func Test002_RetentionBadConfigFileInitially(t *testing.T) {
	ConfigFileName = "retention_policy_test.go"
	r := NewRetentionPolicy()
	assert.Error(t, r.Load())
}

func Test003_RetentionConfigFileBadObjectTypeInitially(t *testing.T) {
	ConfigFileName = "retention_policy_bad_object_type.json"
	r := NewRetentionPolicy()
	assert.Error(t, r.Load())
}

func Test004_RetentionConfigFileSuccess(t *testing.T) {

	//load file
	ConfigFileName = "retention_policy.json"
	r := NewRetentionPolicy()

	tmod := r.LastModTime

	assert.NoError(t, r.Load())
	assert.Equal(t, uint32(90), r.RetentionInDays["1111"])
	assert.NotEqual(t, tmod, r.LastModTime)
	tmod = r.LastModTime

	//file not found
	ConfigFileName = "no_such_file.json"
	assert.NoError(t, r.Load())
	assert.Equal(t, uint32(90), r.RetentionInDays["1111"])

	//bad format
	ConfigFileName = "retention_policy_test.go"
	assert.NoError(t, r.Load())
	assert.Equal(t, uint32(90), r.RetentionInDays["1111"])

	//bad json
	ConfigFileName = "retention_policy_bad_object_type.json"
	assert.NoError(t, r.Load())
	assert.Equal(t, uint32(90), r.RetentionInDays["1111"])

	//bad json
	ConfigFileName = "retention_policy_bad_object_type.json"
	assert.NoError(t, r.Load())
	assert.Equal(t, uint32(90), r.RetentionInDays["1111"])

	assert.Equal(t, tmod, r.LastModTime)

	//
	ConfigFileName = "temp_file.json"
	of, _ := os.Create(ConfigFileName)
	of.WriteString(`{"RetentionInDays" : {"1111" : 92,"2222" : 30,"3333" : 365}}}`)
	of.Close()

	assert.NoError(t, r.Load())
	assert.Equal(t, uint32(92), r.RetentionInDays["1111"])

	assert.NotEqual(t, tmod, r.LastModTime)

	os.Remove(ConfigFileName)
}

func Test005_RetentionPolicyDate(t *testing.T) {
	ConfigFileName = "retention_policy.json"
	r := NewRetentionPolicy()
	assert.NoError(t, r.Load())

	assert.Equal(t, uint32(365), r.Get("3333"))
	assert.Equal(t, uint32(365), uint32(time.Since(r.GetPastDate("3333")).Hours())/24)

	assert.Equal(t, uint32(0), r.RetentionInDays["1112"])
	assert.Equal(t, uint32(90), r.Get("1112"))
	assert.Equal(t, uint32(90), uint32(time.Since(r.GetPastDate("1112")).Hours())/24)

}

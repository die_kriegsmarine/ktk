package filegc

import (
	"github.com/stretchr/testify/assert"
	"io/ioutil"
	"os"
	"strings"
	"testing"
	"time"
)

var (
	cwd, _        = os.Getwd()
	TestRetireDir = cwd + "/tmpretire"
)

type customExpirer struct {
	minutes int32
}

//
func (c customExpirer) Get(companyID string) uint32 {
	return uint32(c.minutes / 60 / 24)
}

//
func (c customExpirer) GetPastDate(companyID string) time.Time {
	// return time.Now().UTC().Add(time.Minute * time.Duration(-c.minutes))
	tm := OneEightyDaysAgo.Add(time.Hour * 180 * 24)
	now := time.Date(tm.Year(), tm.Month(), tm.Day(), 0, 0, 0, 0, time.UTC)
	return now.Add(time.Minute * time.Duration(c.minutes) * (-1))
}

//
func noEntry(path string) bool {
	if _, err := os.Stat(path); err != nil {
		_, ok := err.(*os.PathError)
		return ok
	}

	return false
}

//
func countDirEntries(dirname string) int {
	if entries, err := ioutil.ReadDir(dirname); err != nil {
		panic(err)
	} else {
		return len(entries)
	}
}

func mkrdir() error {
	return os.MkdirAll(TestRetireDir, os.ModePerm)
}

func Test001_MarkerMove(t *testing.T) {
	assert.NoError(t, mkdirs())
	assert.NoError(t, mkrdir())

	m := NewLoggingMarker(TestRootDir, TestRetireDir, NewRetentionPolicy())
	m.Move()

	// fails because it shouldn't be empty
	assert.Error(t, os.Remove(TestRetireDir))
	assert.NoError(t, os.RemoveAll(TestRetireDir))
	assert.NoError(t, os.RemoveAll(TestRootDir))
}

//
func Test002_AlgorithmOneMinuteMove(t *testing.T) {

	assert.NoError(t, mkdirs())
	assert.NoError(t, mkrdir())

	exp := customExpirer{179*24*60 + 23*60 + 59}
	m := NewMarker(TestRootDir, TestRetireDir, exp)
	m.Move()

	assert.Equal(t, true, noEntry(PathCombined+"/00/00"))
	assert.Equal(t, 1, countDirEntries(TestRetireDir))

	assert.NoError(t, os.RemoveAll(TestRetireDir))
	assert.NoError(t, os.RemoveAll(TestRootDir))
}

//
func Test003_AlgorithmFiveMinutesMove(t *testing.T) {

	assert.NoError(t, mkdirs())
	assert.NoError(t, mkrdir())

	exp := customExpirer{179*24*60 + 23*60 + 55}
	m := NewMarker(TestRootDir, TestRetireDir, exp)
	m.Move()

	assert.Equal(t, true, noEntry(PathCombined+"/00/04"))
	assert.Equal(t, 5, countDirEntries(TestRetireDir))

	assert.NoError(t, os.RemoveAll(TestRetireDir))
	assert.NoError(t, os.RemoveAll(TestRootDir))
}

//
func Test004_AlgorithmOneHourMove(t *testing.T) {

	assert.NoError(t, mkdirs())
	assert.NoError(t, mkrdir())

	exp := customExpirer{179*24*60 + 23*60}
	m := NewMarker(TestRootDir, TestRetireDir, exp)
	m.Move()

	assert.Equal(t, true, noEntry(PathCombined+"/00"))
	assert.Equal(t, 1, countDirEntries(TestRetireDir))

	assert.NoError(t, os.RemoveAll(TestRetireDir))
	assert.NoError(t, os.RemoveAll(TestRootDir))
}

//
func Test005_AlgorithmOneHour30MinutesMove(t *testing.T) {

	assert.NoError(t, mkdirs())
	assert.NoError(t, mkrdir())

	exp := customExpirer{179*24*60 + 22*60 + 30}
	m := NewMarker(TestRootDir, TestRetireDir, exp)
	m.Move()

	assert.Equal(t, true, noEntry(PathCombined+"/00"))
	assert.Equal(t, true, noEntry(PathCombined+"/01/29"))
	assert.Equal(t, 31, countDirEntries(TestRetireDir))

	assert.NoError(t, os.RemoveAll(TestRetireDir))
	assert.NoError(t, os.RemoveAll(TestRootDir))
}

//
func Test006_AlgorithmOneDayMove(t *testing.T) {

	assert.NoError(t, mkdirs())
	assert.NoError(t, mkrdir())

	exp := customExpirer{178 * 24 * 60}
	m := NewMarker(TestRootDir, TestRetireDir, exp)
	m.Move()

	pp := strings.Split(PathCombined, "/")
	assert.Equal(t, false, noEntry(strings.Join(pp[0:len(pp)-1], "/")))
	assert.Equal(t, true, noEntry(PathCombined))
	assert.Equal(t, 1, countDirEntries(TestRetireDir))

	assert.NoError(t, os.RemoveAll(TestRetireDir))
	assert.NoError(t, os.RemoveAll(TestRootDir))
}

//
func Test007_AlgorithmOneMonthMove(t *testing.T) {

	assert.NoError(t, mkdirs())
	assert.NoError(t, mkrdir())

	exp := customExpirer{155 * 24 * 60}
	m := NewMarker(TestRootDir, TestRetireDir, exp)
	m.Move()

	pp := strings.Split(PathCombined, "/")
	assert.Equal(t, false, noEntry(strings.Join(pp[0:len(pp)-2], "/")))
	assert.Equal(t, true, noEntry(strings.Join(pp[0:len(pp)-1], "/")))
	assert.Equal(t, 1, countDirEntries(TestRetireDir))

	assert.NoError(t, os.RemoveAll(TestRetireDir))
	assert.NoError(t, os.RemoveAll(TestRootDir))
}

//
func Test008_AlgorithmOneYearMove(t *testing.T) {

	assert.NoError(t, mkdirs())
	assert.NoError(t, mkrdir())

	exp := customExpirer{-365 * 24 * 60}
	m := NewMarker(TestRootDir, TestRetireDir, exp)
	m.Move()

	pp := strings.Split(PathCombined, "/")
	assert.Equal(t, false, noEntry(strings.Join(pp[0:len(pp)-3], "/")))
	assert.Equal(t, true, noEntry(strings.Join(pp[0:len(pp)-2], "/")))
	assert.Equal(t, 1, countDirEntries(TestRetireDir))

	assert.NoError(t, os.RemoveAll(TestRetireDir))
	assert.NoError(t, os.RemoveAll(TestRootDir))
}

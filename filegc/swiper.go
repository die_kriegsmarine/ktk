package filegc

import (
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"time"
)

const (

	// NumberOfDevices is a maximum expected number of devices per box.
	NumberOfDevices = 200
	// NumberOfFilesPerMinute is a number of files, collected for a device for one minute
	NumberOfFilesPerMinute = 50
	// IOOpRate says how quickly a single item of all minute-worth of data for all devices should be removed from disk.
	// All devices will be cleaned up in 30 seconds.
	IOOpRate = (30 * time.Second) / (NumberOfDevices*NumberOfFilesPerMinute + NumberOfDevices)
)

// Swiper is a second pass, which actually removes all the data.
type Swiper struct {
	// RetiredRootDir holds all the entries to be removed (dirs or files).
	RetiredRootDir string
	// ConstantLoad tells if the IO load should be evenly distributed within interval to swipe data,
	// or be in a single burst.
	ConstantLoad bool
}

// removeRecursive removes directory entries recursively. It will not count disk reads (direcory listing)
// as expensive operations.
func (s *Swiper) removeRecursive(fileInfo *os.FileInfo, level uint) {

	tc := time.NewTicker(IOOpRate)

	os.Chdir((*fileInfo).Name())

	if entries, err := ioutil.ReadDir("."); err == nil {
		for _, entry := range entries {
			if entry.IsDir() {
				s.removeRecursive(&entry, level+1)
			}
			if err := os.Remove(entry.Name()); err != nil {
				log.Println("ERROR, failed to remove an entry", err)
			}

			// do not more often but as configured for constant load
			if s.ConstantLoad {
				select {
				case <-tc.C:
				}
			}

		}
	} else {
		fmt.Println("Error reading dir:", err)
	}

	os.Chdir("..")
}

// Clean is an entry point for a Swipe pass. It will not fail if RetiredRootDir is not found.
func (s *Swiper) Clean() {
	started := time.Now()
	if retireDir, err := os.Lstat(s.RetiredRootDir); err != nil {
		log.Println("Error, can't access retired data dir", s.RetiredRootDir, ":", err)
		return
	} else if os.Chdir(s.RetiredRootDir+"/..") == nil {
		s.removeRecursive(&retireDir, 0)
		log.Println("swipe of", retireDir.Name(), "completed in", time.Since(started))
	}
}

// Package filegc contains components of the cleanup agent: marker, swiper, retention policy.
package filegc

import (
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"strconv"
	"time"
)

// Marker implements the first pass: marks expired resources.
// Resources are directories and marking a resource for removal means moving directory to a retirement location
// for swiping.
type Marker struct {
	// DataRootDir is the root dir for data storage
	DataRootDir string
	// RetiredRootDir is an expired resources retirement location, before getting swept.
	RetiredRootDir  string
	retentionPolicy Expirer
	logDebug        bool
	// startedNano help creating unique file names when marking expired resources
	startedNano int64
}

const (
	levelstring = "YMDhm "
)

// NewMarker creates new Marker instance without debug logging
func NewMarker(dataRootDir string, retiredRootDir string, retentionPolicy Expirer) *Marker {
	return &Marker{dataRootDir, retiredRootDir, retentionPolicy, false, time.Now().UnixNano()}
}

// NewLoggingMarker creates new Marker instance with debug logging
func NewLoggingMarker(dataRootDir string, retiredRootDir string, retentionPolicy Expirer) *Marker {
	return &Marker{dataRootDir, retiredRootDir, retentionPolicy, true, time.Now().UnixNano()}
}

// Move is a top-level function of Marker: it handles all companies at a storage location, marking expired data
// for swiping
func (marker *Marker) Move() {

	// make sure to popd at the end
	curdir, err := os.Getwd()
	if err != nil {
		log.Println("Error", err)
		return
	}
	defer os.Chdir(curdir)

	if err := os.Chdir(marker.DataRootDir); err != nil {
		panic(err)
	}

	if companies, err := ioutil.ReadDir("."); err != nil {
		panic(err)
	} else {
		// walk through all companies
		started := time.Now().UnixNano()
		for _, v := range companies {
			marker.cleanup(&v)
		}
		log.Println("All companies finished in", float64(time.Now().UnixNano()-started)/1000/1000/1000, "seconds")
	}

}

// cleanup handles a single company. It figures out the retention interval and starts walking on a per-device
// basis down the tree.
func (marker *Marker) cleanup(fileInfo *os.FileInfo) {

	oldest := marker.retentionPolicy.GetPastDate((*fileInfo).Name())
	log.Println("Retiring older than", oldest, "for company", (*fileInfo).Name())

	os.Chdir((*fileInfo).Name())
	defer os.Chdir("..")

	// timestack is a structure to facilitate recursion-based algorithm
	timestack := [5]int{oldest.Year(), int(oldest.Month()), oldest.Day(), oldest.Hour(), oldest.Minute()}

	if devices, err := ioutil.ReadDir("."); err == nil {
		for _, device := range devices {
			os.Chdir(device.Name())
			if deviceIDs, err := ioutil.ReadDir("."); err == nil {
				for _, deviceID := range deviceIDs {
					marker.retire(&deviceID, timestack[0:len(timestack)], 0)
				}
			}
			os.Chdir("..")
		}
	}
}

// retire is a recursion-based simple algorithm. It handles an individual directory entry
// and either descends further or steps up/iterates on next.
// It will not handle individual files ever as it operates on a minute-worth directory
// of data as a smallest item.
// All expired data is moved to a retirement location for the next swipe step.
func (marker *Marker) retire(fileInfo *os.FileInfo, olderThan []int, level uint) {

	if marker.logDebug {
		fmt.Println("Switching to", (*fileInfo).Name())
	}

	os.Chdir((*fileInfo).Name())

	if entries, err := ioutil.ReadDir("."); err == nil {
		for _, entry := range entries {
			if marker.logDebug {
				fmt.Println("Processing", levelstring[level:level+1], entry.Name())
			}
			if entry.IsDir() {
				m, _ := strconv.ParseInt(entry.Name(), 10, 16)
				if int(m) < olderThan[0] {
					marker.startedNano++
					target := fmt.Sprintf("%v/%x_%v", marker.RetiredRootDir, marker.startedNano, entry.Name())
					if marker.logDebug {
						fmt.Println("Move", levelstring[level:(level+1)], entry.Name(), "=>", target)
					}
					if err := os.Rename(entry.Name(), target); err != nil {
						fmt.Println("ERROR! failed to move", err)
					}
				} else if int(m) == olderThan[0] && len(olderThan) > 1 {
					marker.retire(&entry, olderThan[1:len(olderThan)], level+1)
				} else {
					break
				}
			}
		}
	} else {
		fmt.Println("Error", err)
	}

	os.Chdir("..")

}

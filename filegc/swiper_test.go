package filegc

import (
	"fmt"
	"github.com/stretchr/testify/assert"
	"os"
	"strings"
	"testing"
	"time"
)

const (
	TestRootDir = "tmp"
)

var (
	OneEightyDaysAgo = time.Now().UTC().Add(time.Hour * (-24) * 180)
	Path             = []string{"333", "m_foo_bar_1234", "777", fmt.Sprintf("%04v", OneEightyDaysAgo.Year()), fmt.Sprintf("%02v", int64(OneEightyDaysAgo.Month())), fmt.Sprintf("%02v", OneEightyDaysAgo.Day())}
	PathCombined     = TestRootDir + "/" + strings.Join(Path, "/")
)

func mkdirs() error {

	for hour := 0; hour < 24; hour++ {
		for minute := 0; minute < 60; minute++ {
			os.MkdirAll(fmt.Sprintf("%v/%02v/%02v", PathCombined, hour, minute), os.ModePerm)
		}
	}

	for _, fname := range []string{PathCombined + "/test_file.txt", PathCombined + "/02/02/test_level_b_file.txt"} {
		f, err := os.Create(fname)
		if err != nil {
			return err
		}
		f.WriteString("hello world")
		f.Close()
	}
	return nil
}

// swipe on the actual mock 'data' dir
func Test001_SwiperClean(t *testing.T) {

	started := time.Now()
	assert.NoError(t, mkdirs())
	fmt.Println("Create finished in", time.Since(started))

	_, err := os.Lstat(PathCombined + "/02/02")
	assert.NoError(t, err)

	started = time.Now()
	sw := Swiper{TestRootDir, false}
	sw.Clean()
	fmt.Println("Swipe finished in", time.Since(started))

	_, err = os.Lstat(PathCombined + "/02/02")
	assert.Error(t, err)

	assert.NoError(t, os.Remove(TestRootDir))
}

// swipe on the actual mock 'data' dir
func Test002_SwiperBadRetireDir(t *testing.T) {

	sw := Swiper{TestRootDir, false}
	sw.Clean()
}

//
func Test003_SwiperTestConstantLoad(t *testing.T) {
	assert.NoError(t, os.MkdirAll(TestRetireDir+"/abc/edf", os.ModePerm))
	sw := Swiper{TestRetireDir, true}
	sw.Clean()
	assert.NoError(t, os.Remove(TestRetireDir))

}

package filegc

import (
	"encoding/json"
	"log"
	"os"
	"time"
)

const (
	defaultRetentionPolicy = 90
)

var (
	// ConfigFileName is a retention policy configuration. It's a simple JSON map[string]uint32
	ConfigFileName = "retention_policy.json"
	noTm           = time.Time{}
)

// Expirer is an interface which defines mapping of a companyID to it's data retention policy (in days)
// and also can generate a date in the past, corresponded to retention value.
type Expirer interface {
	// Get returns retention policy (in days) for a company
	Get(companyID string) uint32
	// GetPastDate returns last retention time.Date for a company
	GetPastDate(companyID string) time.Time
}

// RetentionPolicy is an implementation of Expirer, back up by JSON config file
type RetentionPolicy struct {
	// RetentionInDays is a map company_id to retention value (in days)
	RetentionInDays map[string]uint32
	// LastModTime of the configuration file. Not set until config is parsed for the first time
	LastModTime time.Time
}

// NewRetentionPolicy makes a new RetentionPolicy instance
func NewRetentionPolicy() *RetentionPolicy {
	return &RetentionPolicy{RetentionInDays: make(map[string]uint32)}
}

// Load tries to load the configuration. It will only fail if no configuration was previously loaded.
// It will not re-load config if it was not changed. It will complain if file does not exist or malformed,
// but will not fail if valid config was previously loaded.
func (r *RetentionPolicy) Load() error {
	if stat, err := os.Stat(ConfigFileName); err != nil {
		if r.LastModTime == noTm {
			return err
		}
	} else if file, err := os.Open(ConfigFileName); err != nil {
		log.Println("Error", err)
		if r.LastModTime == noTm {
			return err
		}
	} else {

		defer file.Close()

		decoder := json.NewDecoder(file)
		err := decoder.Decode(r)
		if err != nil {
			log.Println("Error reading retention policy file:", err)
			if r.LastModTime == noTm {
				return err
			}
		} else {
			r.LastModTime = stat.ModTime()
		}
	}
	return nil
}

// Get returns an explicitly configured data retention value (in days) for a company, or a default one.
func (r *RetentionPolicy) Get(companyID string) uint32 {
	if v, ok := r.RetentionInDays[companyID]; ok {
		return v
	}
	return defaultRetentionPolicy
}

// GetPastDate returns a date in the past, corresponding to Get() return value
func (r *RetentionPolicy) GetPastDate(companyID string) time.Time {
	now := time.Now().UTC()
	return now.Add(time.Hour * (-24) * time.Duration(r.Get(companyID)))
}

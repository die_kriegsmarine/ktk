// The MIT License (MIT)
//
// Copyright (c) 2015 Alexander Krassiev
//
//  Permission is hereby granted, free of charge, to any person obtaining a
//  copy of this software and associated documentation files (the "Software"),
//  to deal in the Software without restriction, including without limitation
//  the rights to use, copy, modify, merge, publish, distribute, sublicense,
//  and/or sell copies of the Software, and to permit persons to whom the
//  Software is furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
//  OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
//  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//  DEALINGS IN THE SOFTWARE.

// Package main launches the cleanup agent.
//
// The cleanup agent monitors data storage directory and cleans expired resources up per retention policy.
// Policy is configured via config file (json format), located in the same directoty as binary.
//
// The primary mode of running is daemon (background/etc). The process can be stoped and re-started as needed.
// It will catch up with late-to-clean data. Stopping leaves no inconsistent state as the state is not maintained
// except for marked for clean up data, which is persisted on disk (directory moved to a clean up location).
//
// Configuration can be updated as needed and new valid configuration will be picked up at the next cleanup cycle.
//
// The algorithm is not a generic one, it assumes a specific directory layout for storing resources:
// <storage_root_dir>/:company_id/:device_name/:device_id/:year/:month:/day:/:hour/:minute/<data_files>
//
// The main algorithm is similar to garbage collection in an GC-capable programming language: mark & swipe.
//
//
package main

import (
	"bitbucket.org/die_kriegsmarine/ktk/filegc"
	"flag"
	"log"
	"time"
)

const (
	// DataRootDir is the root dir for data storage
	DataRootDir = "/Users/alexkr/tmp/ktk/active"
	// RetireIntervalMinutes tells how often the cleanup agent should run.
	RetireIntervalMinutes = 1
	// RetiredRootDir is an expired resources retirement location, before getting swept.
	RetiredRootDir = "/Users/alexkr/tmp/ktk/retired"
)

var configFile = flag.String("config", filegc.ConfigFileName, "non-standard config file path")

// main can be interrupted at any time by sigint/sigquit.
func main() {

	flag.Parse()
	filegc.ConfigFileName = *configFile

	// clean up as often
	tc := time.NewTicker(RetireIntervalMinutes * 60 * time.Second)
	// retention policy configuration
	retentionPolicy := filegc.NewRetentionPolicy()
	// mark pass: mark resources to be cleaned up
	marker := filegc.NewMarker(DataRootDir, RetiredRootDir, retentionPolicy)
	// swipe pass
	swiper := filegc.Swiper{RetiredRootDir: RetiredRootDir, ConstantLoad: true}

	for {

		// idle cycles if config has never been loaded.
		// The algorithm should be more smart if cleanup happens infrequently, for instance,
		// monitor config file change or reload on some signal (SIGUSR1, etc)
		//
		if retentionPolicy.Load() == nil {

			// mark dirs for retirement (move them)
			// When all caught up, it's extremely quick to mark data for 200 devices.
			// A bit longer if it needs to catch up (or rentention reduced), but still quick
			// as it needs not too many disk reads and marks whole dir trees.
			marker.Move()

			// swipe: remove marked files. The speed of swiping should ideally be a function of a
			// disk access speed and data receive rate.
			// For the illustration purpose, all data for all devices for one minute worth is
			// removed with 30 seconds.
			// As a ballpark estimate, on an idle laptop's SSD, walking recursively one year worth
			// of minute-resolution data takes about 12-14 seconds; removing is extra.
			swiper.Clean()

		} else {
			log.Println("Warning: failed to load config file", filegc.ConfigFileName)
		}

		// do not swipe more often than configured (every minute for now). Fast-forward to the next
		// cycle if the current one took too long.
		select {
		case <-tc.C:
		}
	}

}

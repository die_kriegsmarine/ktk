# A storage cleanup agent

## Prerequisites

Golang 1.3+ should be installed on a system. Download and install Go from [here](http://golang.org/doc/install). These instructions assume using a UNIX-like OS for simplicity.

## Getting and running the source code

Make sure your `GOPATH` is set. In terminal window, issue:

>	`go get bitbucket.org/die_kriegsmarine/ktk`

Now you can run the code with:

>	`$GOPATH/bin/ktk --config=$GOPATH/src/bitbucket.org/die_kriegsmarine/ktk/retention_policy.json`


## Implementation notes

Between various approaches the simplest one was selected. It mimics GC operation (mark & swipe) and is a two-pass algorithm. Marking moves expired data directories away from under the storage dir; swiping just removes
old entries from a disk. Essentially, the state (which entries are to be removed) is maintained via file system, which is highly resilient as `rename()` system call guarantees consistency even across system crashes (on some FS types). For that to happen though, both data storage dir and data retirement dir should reside on the same FS for the rename() to be resilient and fast (the call retains the same inode for the entry).

The two-pass algorithm can actually be modified and have stages separated, so marking will run independently of swiping. Data access process (SQL, etc) can benefit from the separation as it will never see and have to process expired data as the mark pass is very fast to move expired data away.

Swipe pass is the slowest one as it actually makes the builk of writes to the disk. During the stable operations, when data is expired regularly (once a minute), the swipe has to operate on approximately 200*50+200 directory entries (10200) at worse. It is stlil quite manageable load. To level out the IO load during the cycle, the agent does as many as one heavy write operation during the swipe pass per X milliseconds (i.e. at worse, all 10200 entries should be removed in under 60 seconds to keep up with the once a minute schedule). The rate of swiping is hardcoded and can be changed.

If agent needs to catch up or account for a changed retaining policy, the marking pass will still be quick, while swiping will take linearly more time. The marking will not run until swiping completes for this given implementation (see pass separation remark above).

The actual speed of operation depends on a disk speed and possibly on a filesystem type (to the less extent) and a system load. 

The agent is not designed to operate on cron. Instead, it launches and runs (daemonized in a best case) in a background. The suggested iteration mode is one cycle per minute (now harcoded in main.go) to handle the load evenly during the day. 

## Optimizations

There can be a little gain to arrange directories in less hierarcical fashion for data storage, i.e. put all device flies into a single dir, such as all files for one month in single dir. Such approach may speed up the marking pass as directories are read in one transfer from disk, but the gain is insignifican on just 200 devices per box and even more insignificant for once-a-minute cycle.

For the mark pass, it might also be a little quicker to build initially in-memory structure of the storage sub-directory tree and monitor changes via inotify (linux), hence saving some cycles on a mark pass. But added complexity may not justify the savings.

Under heavy IO it might be tempting to build a system, which receives directory change notifications from outside (i.e. from a message queue, etc) so no actual disk read is needed as the tree structure is always known (once the agent started and scanned the subtree). But added complexity also does not justify the effort (i.e. to walk the subtree for the one year worth of device data is much faster then actual data removal; walking takes less a minute on my laptop while swiping takes much much longer).

So I after deliberating on various options for this specific problem I favored the presented solution as the simplest one. Further imprvemens can be made based on actual system behavior and observations.

## Implementation note.

One of the requirements states that the structure is <company>/<device>/<year>/... etc, while the example shows <company_id>/<device_name>/<device_id>/<year>/... etc. I assumed there might be several device_ids per device [name].


# Testing notes

Code testing converage is 87.6% on filegc package; main is not convered by unit tests.


# License

The MIT License (MIT)

Copyright (c) 2015 Alexander Krassiev

 Permission is hereby granted, free of charge, to any person obtaining a
 copy of this software and associated documentation files (the "Software"),
 to deal in the Software without restriction, including without limitation
 the rights to use, copy, modify, merge, publish, distribute, sublicense,
 and/or sell copies of the Software, and to permit persons to whom the
 Software is furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 DEALINGS IN THE SOFTWARE.

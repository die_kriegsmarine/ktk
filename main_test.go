package main

import (
	"fmt"
	randomdata "github.com/Pallinder/go-randomdata"
	"os"
	"strings"
	"testing"
	"time"
)

const (
	TopDir      = "/Users/alexkr/tmp/ktk"
	MockDevices = 1 //200
	MockYears   = 4
)

func mkdircd(dirnamenum int, format string) {

	dirname := fmt.Sprintf(format, dirnamenum)

	if os.Mkdir(dirname, os.ModePerm) != nil || os.Chdir(dirname) != nil {
		panic(fmt.Errorf("can't mkdir or chdir %v", dirname))
	}
}

func MockDirs() {

	counter := 0
	const max = MockDevices * 4 * 12 * 31 * 24 * 60

	started := time.Now().Unix()

	for f := 1; f < MockDevices+1; f++ {
		dirname := fmt.Sprintf("%v/%v/%v/%v/%v/%v", DataRootDir, randomdata.Number(1024*1024), string(randomdata.Number(int('a'), int('z'))), randomdata.Number(215), strings.ToLower(randomdata.SillyName()), randomdata.Number(1024))
		if err := os.MkdirAll(dirname, os.ModePerm); err != nil {
			panic(err)
		}

		if os.Chdir(dirname) != nil {
			panic(fmt.Errorf("can't mkdir or chdir %v", dirname))
		}

		for year := 2013; year < 2016; year++ {

			mkdircd(year, "%v")

			for month := 1; month < 13; month++ {
				mkdircd(month, "%02v")

				for day := 1; day < 32; day++ {
					mkdircd(day, "%02v")

					for hour := 0; hour < 24; hour++ {
						mkdircd(hour, "%02v")
						for minute := 0; minute < 60; minute++ {
							if err := os.Mkdir(fmt.Sprintf("%02v", minute), os.ModePerm); err != nil {
								panic(err)
							}

							counter++
							if counter%(1024*64) == 0 {
								elapsed := time.Now().Unix() - started
								fmt.Println(float32(counter)/max, ":", elapsed, "sec,", float32(elapsed)/float32(counter)*float32(max-counter), "remains")
							}
						}
						os.Chdir("..")
					}
					os.Chdir("..")
				}
				os.Chdir("..")
			}
			os.Chdir("..")
		}
	}
}

//
func Test001_main(t *testing.T) {
}
